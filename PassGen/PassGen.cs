﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Collections;
using System.IO;
using System.Runtime.Serialization;
using System.Security.Permissions;
using System.Xml.Serialization;
using System.Xml;

namespace PassGen
{
    public partial class PassGen : Form
    {

        SortedDictionary<String, Key> keys = new SortedDictionary<String, Key>();

        static char[] AZ = { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        static char[] az = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

        static char[] numb = { '1', '2', '3', '4', '5', '6', '7', '8', '9', '0' };

        static char[] symb = { '!', '@', '#', '$', '%', '^', '&', '*' };

        public PassGen()
        {
            InitializeComponent();

            String directory = Application.UserAppDataPath;

            directory += @"\keys";

            Directory.CreateDirectory(directory);

            //TODO Import keys
            try
            {
                keys = loadData();
            }
            catch (IOException e)
            {

                MessageBox.Show("Either no file exists, or you did not pick a file" +
                    ". The program will continue without loading a file.", "BITA PassGen Open File Dialog", MessageBoxButtons.OK, 
                    MessageBoxIcon.Information);

                keys.Add("Default",new Key());
            }

            this.KeyBox.DataSource = keys.Keys.ToList();
            
            for(int n=0; n<4; n++) this.PassProperties.SetSelected(n, true);


        }

        private void Password_TextChanged(object sender, EventArgs e)
        {

            if(!string.IsNullOrWhiteSpace(this.KeywordBox.Text)
                && !string.IsNullOrWhiteSpace(this.PasswordBox.Text))
            {


                this.textBox1.Text = generatePassword();


            }

        }

        private void AddButton_Click(object sender, EventArgs e)
        {

            Key newKey = new Key();

            newKey.key = this.KeywordBox.Text;
            newKey.description = this.DescriptionBox.Lines;
            newKey.length = (int)this.PassLengthBox.Value;
            newKey.usesLowercase = this.PassProperties.GetItemChecked(1);
            newKey.usesNumbers = this.PassProperties.GetItemChecked(2);
            newKey.usesSpecialChars = this.PassProperties.GetItemChecked(3);
            newKey.usesUppercase = this.PassProperties.GetItemChecked(0);

            if (!this.keys.ContainsKey(newKey.key))
            {
                this.keys.Add(newKey.key, newKey);
                this.KeyBox.DataSource = this.keys.Keys.ToList();
                this.KeyBox.Refresh();
            }
            else
            {
                this.keys.Remove(newKey.key);
                this.keys.Add(newKey.key, newKey);
                this.KeyBox.DataSource = this.keys.Keys.ToList();
                this.KeyBox.Refresh();
            }
            
            
            

            
        }

        private void SaveButton_Click(object sender, EventArgs e)
        {
            try
            {
                saveData(this.keys);
            }
            catch (IOException err)
            {
                MessageBox.Show("A bad thing happened! Try again.", "IO Error", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);
            }



        }

        private void LoadButton_Click(object sender, EventArgs e)
        {

            try
            {
               SortedDictionary<String, Key> val;
                val = loadData();
                this.keys = val;
            }
            catch (IOException err)
            {
                MessageBox.Show("A bad thing happened! Try again.", "IO Error", MessageBoxButtons.OK,
                   MessageBoxIcon.Error);
            }

            
            this.KeyBox.DataSource = this.keys.Keys.ToList();
            this.KeyBox.Refresh();
        }

        private void DelButton_Click(object sender, EventArgs e)
        {

            this.keys.Remove((String)this.KeyBox.SelectedValue);
            
            this.KeyBox.DataSource = this.keys.Keys.ToList();
            this.KeyBox.Refresh();


        }

        private void KeySelectedFromList(object sender, EventArgs e)
        {

            String key = (String)this.KeyBox.SelectedValue;

            Key disp;

            this.keys.TryGetValue(key, out disp);

            this.KeywordBox.Text = disp.key;
            this.PassLengthBox.Value = (decimal)disp.length;
            this.PassProperties.SetItemChecked(0, disp.usesUppercase);
            this.PassProperties.SetItemChecked(1, disp.usesLowercase);
            this.PassProperties.SetItemChecked(2, disp.usesNumbers);
            this.PassProperties.SetItemChecked(3, disp.usesSpecialChars);
            this.DescriptionBox.Lines = disp.description;

            if (!string.IsNullOrWhiteSpace(this.KeywordBox.Text)
                && !string.IsNullOrWhiteSpace(this.PasswordBox.Text))
            {


                this.textBox1.Text = generatePassword();


            }


        }

        private void DataSourceChangeHandle(object sender, EventArgs e)
        {
           
            this.KeyBox.DataSource = this.keys.Keys.ToList();
            this.KeyBox.Refresh();


        }

        private SortedDictionary<String, Key> loadData()
        {

            SortedDictionary<String, Key> dic = new SortedDictionary<String, Key>();
            List<Key> list = new List<Key>();

            XmlSerializer ser = new XmlSerializer(typeof(KeyBox));


            OpenFileDialog open = new OpenFileDialog();
            open.Filter = "PassGen Generated XML files|*.xml|All files|*.*";
            open.RestoreDirectory = true;
            open.FilterIndex = 1;
            
            String directory = Application.UserAppDataPath;

            directory += @"\keys";

            open.InitialDirectory = directory;

            if (open.ShowDialog() == DialogResult.OK)
            {

                BufferedStream stream = new BufferedStream(open.OpenFile());

                if (stream != null)
                {
                    list = ((KeyBox)ser.Deserialize(stream)).box;
                }
                else
                {

                    throw new IOException("IO Failure");

                }

                stream.Close();


            }
            else
            {

                throw new IOException("IO Failure");

            }

            foreach (Key k in list)
            {

                dic.Add(k.key, k);

            }

            return dic;

        }

        private void saveData(SortedDictionary<String, Key> output)
        {

            XmlSerializer ser = new XmlSerializer(typeof(KeyBox));

            SaveFileDialog save = new SaveFileDialog();

            List<Key> list = new List<Key>();

            foreach(KeyValuePair<String, Key> e in this.keys){

                list.Add(e.Value);

            }

            KeyBox box;
            box.box = list;

            save.Filter = "PassGen Generated XML files|*.xml|All files|*.*";
            save.FilterIndex = 1;

            String directory = Application.UserAppDataPath;

            directory += @"\PassGen\keys";

            save.InitialDirectory = directory;

            if (save.ShowDialog() == DialogResult.OK)
            {

                BufferedStream stream = new BufferedStream(save.OpenFile());

                if (stream != null)
                {

                    ser.Serialize(stream, box);

                    

                }
                else
                {

                    throw new IOException("IO Failure");

                }

                stream.Close();
            }
            else
            {

                throw new IOException("IO Failure");

            }


        }

        private String generatePassword()
        {
            StringBuilder strb = new StringBuilder();



            Random rand1 = new Random(getCode(this.KeywordBox.Text));
            Random rand2 = new Random(getCode(this.PasswordBox.Text));

            int bo = this.PassProperties.CheckedIndices.Count;
            int[] bolist;
            int count = 0;
            if (bo != 0)
            {
                bolist = new int[bo];
                foreach (int e in this.PassProperties.CheckedIndices)
                {

                    bolist[count] = e;
                    count++;

                }
            }
            else
            {

                bolist = new int[] {0,2,3,1};
                bo = 4;
            }
            

            

            for (int n = 0; n < (int)this.PassLengthBox.Value; n++)
            {

                int r = rand1.Next(0, bolist.Length);

                int type = bolist[r];

                switch (type)
                {

                    case 0:
                        strb.Append(AZ[rand1.Next(0, 26)]);
                        break;

                    case 1:
                        strb.Append(az[rand2.Next(0, 26)]);
                        break;

                    case 2:
                        strb.Append(numb[rand1.Next(0, 10)]);
                        break;

                    case 3:
                        strb.Append(symb[rand2.Next(0, symb.Length)]);
                        break;

                    default:
                        break;


                }

                
                

            }


            return strb.ToString();
        }

        private Key getCurrentKey()
        {

            String key = (String)this.KeyBox.SelectedValue;

            Key disp;

            this.keys.TryGetValue(key, out disp);

            return disp;

        }

        private int getCode(String input)
        {
            int total = 0;
            int count = 1;

            foreach (char c in input)
            {

                total += count * (int)c;
                count++;


            }
            

            return total;


        }

    }

    [Serializable()]
    public class Key : ISerializable
    {
        //Derived from key field
        public String key { set; get; }
        public bool usesUppercase { set; get; }
        public bool usesLowercase { set; get; }
        public bool usesSpecialChars { set; get; }
        public bool usesNumbers { set; get; }
        //Length of password associated with this key
        public int length { set; get; }

        public String[] description { set; get; }

        public Key()
        {

            this.key = "Default";
            this.usesUppercase = false;
            this.usesLowercase = false;
            this.usesSpecialChars = false;
            this.usesNumbers = false;
            this.length = 8;
            this.description = new String[] {"Put description here."};


        }

        protected Key(SerializationInfo info, StreamingContext context)
        {

            this.key = info.GetString("key");
            this.length = info.GetInt32("Length");
            this.usesLowercase = info.GetBoolean("UpperCase");
            this.usesUppercase = info.GetBoolean("LowerCase");
            this.usesNumbers = info.GetBoolean("Numbers");
            this.usesSpecialChars = info.GetBoolean("SpecialChars");
            //this.description = info.GetString("Desc");

        }

        [SecurityPermission(SecurityAction.Demand, SerializationFormatter = true)]
        public virtual void GetObjectData(SerializationInfo info, StreamingContext context)
        {

            if (info == null)
            {

                throw new ArgumentNullException("info");

            }

            info.AddValue("key", key);

            info.AddValue("UpperCase", usesUppercase);
            info.AddValue("LowerCase", usesLowercase);
            info.AddValue("SpecialChars", usesSpecialChars);
            info.AddValue("Numbers", usesNumbers);

            info.AddValue("Length", length);
            info.AddValue("Desc", description);

        }

        [SecurityPermission(SecurityAction.LinkDemand, Flags = SecurityPermissionFlag.SerializationFormatter)]
        void ISerializable.GetObjectData(SerializationInfo info, StreamingContext context)
        {

            GetObjectData(info, context);

        }


    }

    public struct KeyBox
    {

        public List<Key> box;

    }

}
